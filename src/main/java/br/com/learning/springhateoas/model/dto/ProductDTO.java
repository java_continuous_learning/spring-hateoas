package br.com.learning.springhateoas.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO extends RepresentationModel<ProductDTO> {
  private int code;
  private String description;
  private int quantity;
  private double charge;
}
