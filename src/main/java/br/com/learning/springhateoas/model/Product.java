package br.com.learning.springhateoas.model;

import br.com.learning.springhateoas.model.dto.ProductDTO;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "produto")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "codigo")
  private int code;

  @Column(name = "descricao")
  private String description;

  @Column(name = "quantidade")
  private int quantity;

  @Column(name = "preco")
  private double charge;

}
