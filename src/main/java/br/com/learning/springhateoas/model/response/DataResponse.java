package br.com.learning.springhateoas.model.response;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.tomcat.jni.Local;

@Getter
@Setter
@AllArgsConstructor
public class DataResponse<T> {
  private T data;
  private LocalDateTime timestamp;

  public DataResponse(T data) {
    this.data = data;
    this.timestamp = LocalDateTime.now();
  }
}
