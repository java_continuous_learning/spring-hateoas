package br.com.learning.springhateoas.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import br.com.learning.springhateoas.model.dto.ProductDTO;
import br.com.learning.springhateoas.model.response.DataResponse;
import br.com.learning.springhateoas.service.ProductService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/products")
public class ProductController {

  private ProductService service;

  @Autowired
  public ProductController(ProductService service) {
    this.service = service;
  }

  @PostMapping
  public ResponseEntity<Void> create(@RequestBody ProductDTO newProduct) {
    service.register(newProduct);

    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @GetMapping("/{id}")
  public ResponseEntity<DataResponse<ProductDTO>> getProduct(@PathVariable int id) {
    Optional<ProductDTO> response = service.consult(id);

    if (response.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      response.get().add(linkTo(methodOn(ProductController.class).getAll()).withSelfRel());
    }

    return ResponseEntity.ok().body(new DataResponse<>(response.get()));
  }

  @GetMapping
  public ResponseEntity<DataResponse<List<ProductDTO>>> getAll() {

    List<ProductDTO> products = service.getAllProducts();

    products.forEach(x -> {
      x.add(linkTo(methodOn(ProductController.class).getProduct(x.getCode())).withSelfRel());
    });

    return ResponseEntity.ok().body(new DataResponse<>(products));
  }
}
