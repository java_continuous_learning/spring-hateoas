package br.com.learning.springhateoas.service;

import br.com.learning.springhateoas.converter.DTOConverter;
import br.com.learning.springhateoas.converter.EntityConverter;
import br.com.learning.springhateoas.model.dto.ProductDTO;
import br.com.learning.springhateoas.repository.ProductRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ProductService {

  private  ProductRepository productRepository;

  @Autowired
  public ProductService(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  public void register(ProductDTO product) {
    log.info("Processing for product registration started...");
    productRepository.save(EntityConverter.convertToEntity(product));

    log.info("successfully registered product...");
  }

  public Optional<ProductDTO> consult(int id) {
    return Optional.of(productRepository.findById(id).map(DTOConverter::convertToDTO).orElseThrow());
  }

  public List<ProductDTO> getAllProducts() {
    return productRepository.findAll().stream().map(DTOConverter::convertToDTO).collect(Collectors.toList());
  }
}
