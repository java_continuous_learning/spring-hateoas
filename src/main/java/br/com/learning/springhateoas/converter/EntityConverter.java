package br.com.learning.springhateoas.converter;

import br.com.learning.springhateoas.model.Product;
import br.com.learning.springhateoas.model.dto.ProductDTO;

public class EntityConverter {

  public static Product convertToEntity(ProductDTO product) {
    Product entity = new Product();
    entity.setDescription(product.getDescription());
    entity.setQuantity(product.getQuantity());
    entity.setCharge(product.getCharge());
    return entity;
  }
}
