package br.com.learning.springhateoas.converter;

import br.com.learning.springhateoas.model.Product;
import br.com.learning.springhateoas.model.dto.ProductDTO;

public class DTOConverter {

  public static ProductDTO convertToDTO(Product productEntity) {
    ProductDTO product = new ProductDTO();
    product.setCode(productEntity.getCode());
    product.setDescription(productEntity.getDescription());
    product.setQuantity(productEntity.getQuantity());
    product.setCharge(productEntity.getCharge());
    return product;
  }
}
