package br.com.learning.springhateoas.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.learning.springhateoas.model.dto.ProductDTO;
import br.com.learning.springhateoas.model.response.DataResponse;
import br.com.learning.springhateoas.service.ProductService;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {

  @Mock
  private ProductService serviceMock;

  @InjectMocks
  private ProductController controller;

  @BeforeEach
  public void setUp() {
    this.controller = new ProductController(this.serviceMock);
  }

  @Test
  public void testShouldRegisteredProductSuccessfully() {
    ResponseEntity<Void> response = controller.create(buildRequestBody());

    verify(serviceMock, atLeastOnce()).register(any(ProductDTO.class));
    assertEquals(response.getStatusCode(), HttpStatus.CREATED, "Assert status code");
  }

  @Test
  public void testShouldGetProductSuccessfully() {
    when(serviceMock.consult(anyInt())).thenReturn(Optional.of(new ProductDTO(2, "Monitor", 3, 2000.00)));

    ResponseEntity<DataResponse<ProductDTO>> product = controller.getProduct(1);

    verify(serviceMock, atLeastOnce()).consult(anyInt());
    assertEquals(3, product.getBody().getData().getQuantity(), "");
    assertEquals("Monitor", product.getBody().getData().getDescription(), "");
  }

  private static ProductDTO buildRequestBody() {
    return new ProductDTO(1, "Iphone SE", 34, 2000.00);
  }
}
